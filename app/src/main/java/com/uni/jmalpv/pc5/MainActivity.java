package com.uni.jmalpv.pc5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int random;
    Random r = new Random();
    boolean isChangeColor = false;
    CountDownTimer myCountDownTimer;

    ImageView imageViewClockOne, imageViewClockTwo, imageViewClockThree;
    TextView textViewTimer, textViewNumberOne, textViewNumberTwo, textViewNumberThree;
    Button buttonMinusOne, buttonMinusTwo, buttonMinusThree, buttonPlusOne, buttonPlusTwo, buttonPlusThree, buttonOpen;

    float clockOneRotation = 0, clockTwoRotation = 0, clockThreeRotation = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTimer = findViewById(R.id.text_view_timer);

        imageViewClockOne = findViewById(R.id.clock_one);
        imageViewClockTwo = findViewById(R.id.clock_two);
        imageViewClockThree = findViewById(R.id.clock_three);

        textViewNumberOne = findViewById(R.id.text_view_number_one);
        textViewNumberTwo = findViewById(R.id.text_view_number_two);
        textViewNumberThree = findViewById(R.id.text_view_number_three);

        buttonMinusOne = findViewById(R.id.button_minus_one);
        buttonMinusTwo = findViewById(R.id.button_minus_two);
        buttonMinusThree = findViewById(R.id.button_minus_three);

        buttonPlusOne = findViewById(R.id.button_plus_one);
        buttonPlusTwo = findViewById(R.id.button_plus_two);
        buttonPlusThree = findViewById(R.id.button_plus_three);

        buttonOpen = findViewById(R.id.button_open);

        random = 100 + r.nextInt(900);

        buttonOpen.setText(String.valueOf(random));

        ButtonsSetOnClickListener();

        myCountDownTimer = MyCountDownTimer();

    }

    private int CheckNumber(String numberOneText, String numberTwoText, String numberThreeText) {
        int numCheck = 0;

        if (numberOneText.charAt(0) == String.valueOf(random).charAt(0)) {
            numCheck += 1;
        }
        if (numberTwoText.charAt(0) == String.valueOf(random).charAt(1)) {
            numCheck += 1;
        }
        if (numberThreeText.charAt(0) == String.valueOf(random).charAt(2)) {
            numCheck += 1;
        }

        return numCheck;
    }

    private void ButtonsSetOnClickListener(){
        buttonMinusOne.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberOne.getText().toString());
            num-=1;
            if(num < 0){
                textViewNumberOne.setText("0");
            }
            else {
                textViewNumberOne.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockOne, clockOneRotation,false);
            }
        });

        buttonMinusTwo.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberTwo.getText().toString());
            num-=1;
            if(num < 0){
                textViewNumberTwo.setText("0");
            }
            else {
                textViewNumberTwo.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockTwo, clockTwoRotation,false);
            }
        });

        buttonMinusThree.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberThree.getText().toString());
            num-=1;
            if(num < 0){
                textViewNumberThree.setText("0");
            }
            else {
                textViewNumberThree.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockThree, clockThreeRotation,false);
            }
        });

        buttonPlusOne.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberOne.getText().toString());
            num+=1;
            if(num > 9){
                textViewNumberOne.setText("9");
            }
            else {
                textViewNumberOne.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockOne, clockOneRotation,true);
            }
        });

        buttonPlusTwo.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberTwo.getText().toString());
            num+=1;
            if(num > 9){
                textViewNumberTwo.setText("9");
            }
            else {
                textViewNumberTwo.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockTwo, clockTwoRotation,true);
            }
        });

        buttonPlusThree.setOnClickListener(view -> {
            int num = Integer.parseInt(textViewNumberThree.getText().toString());
            num+=1;
            if(num > 9){
                textViewNumberThree.setText("9");
            }
            else {
                textViewNumberThree.setText(String.valueOf(num));
                StartClockAnimation(imageViewClockThree, clockThreeRotation,true);
            }
        });

        buttonOpen.setOnClickListener(view -> {
            int numCheck = CheckNumber(textViewNumberOne.getText().toString(), textViewNumberTwo.getText().toString(), textViewNumberThree.getText().toString());
            Resources res = getResources();
            if(numCheck == 3){
                myCountDownTimer.cancel();
                Intent intent = new Intent(MainActivity.this, EndActivity.class);
                intent.putExtra("WIN", true);
                startActivity(intent);
            }
            Snackbar.make(view, String.format(res.getString(R.string.snack_bar_msg), numCheck), Snackbar.LENGTH_LONG).show();
        });
    }

    private CountDownTimer MyCountDownTimer(){
        return new CountDownTimer(60100, 1000) {
            @Override
            public void onTick(long l) {

                int seconds = (int) l / 1000;
                int minutes = (int) l / 60000;

                if(seconds < 30) {
                    if(!isChangeColor) {
                        isChangeColor = true;
                        textViewTimer.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.red));
                    }
                    else {
                        isChangeColor = false;
                        textViewTimer.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.black));
                    }
                }
                @SuppressLint("DefaultLocale")
                String formatTime = String.format("%02d:%02d", minutes, seconds);
                textViewTimer.setText(formatTime);
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(MainActivity.this, EndActivity.class);
                intent.putExtra("WIN", false);
                startActivity(intent);
            }
        }.start();
    }

    private void StartClockAnimation(ImageView imageViewClock, float clockRotation, boolean isPlus){
        if(!isPlus) {
            clockRotation += 40f;
        }
        else {
            clockRotation -= 40f;
        }
        imageViewClock.animate().rotation(clockRotation).setDuration(100).start();
    }
/*
    @Override
    protected void onPause(){
        super.onPause();
        try {
            myCountDownTimer.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        myCountDownTimer.start();
    }
 */
}