package com.uni.jmalpv.pc5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EndActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView textViewSplash;
    Animation animationImage;
    Button buttonTryAgain, buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        imageViewSplash = findViewById(R.id.image_view_splash);
        textViewSplash = findViewById(R.id.text_view_splash);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);

        buttonTryAgain = findViewById(R.id.button_again);
        buttonExit = findViewById(R.id.button_exit);

        imageViewSplash.setAnimation(animationImage);

        ButtonsSetOnClickListener();

        CheckWinCondition();
    }

    private void ButtonsSetOnClickListener(){
        buttonTryAgain.setOnClickListener(view -> {
            Intent intent = new Intent(EndActivity.this, MainActivity.class);
            startActivity(intent);
        });

        buttonExit.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(EndActivity.this);
            builder.setTitle(R.string.dialog_title);
            builder.setCancelable(false);
            builder.setMessage(R.string.dialog_msg);
            builder.setPositiveButton("Si", (dialogInterface, i) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            });
            builder.setNegativeButton("No", (dialogInterface, i) -> {
                Intent intent = new Intent(EndActivity.this, StartActivity.class);
                startActivity(intent);
                finish();
            });
            builder.create().show();
        });
    }

    private void CheckWinCondition(){
        boolean isWin = getIntent().getBooleanExtra("WIN", false);
        if (!isWin) {
            imageViewSplash.setImageResource(R.drawable.lose);
            textViewSplash.setText(R.string.text_view_lose);
            buttonTryAgain.setText(R.string.button_text_try_again);
        }
        else {
            imageViewSplash.setImageResource(R.drawable.win);
            textViewSplash.setText(R.string.text_view_win);
            buttonTryAgain.setText(R.string.button_text_play_again);
        }
    }
}